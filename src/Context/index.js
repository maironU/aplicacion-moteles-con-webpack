import React, {useState} from 'react'

const TheContext = React.createContext()
const {Provider, Consumer} = TheContext

const Context = ({children}) => {

	let [filter, setFilter] = useState([])
	let [historyList, setHistoryList] = useState([])
	let [isOpenModal, setIsOpenModal] = useState(false)
	let [id, setId] = useState(0)
	let [Historias, setHistorias] = useState([])
	let [HistoryMotel, setHistoryMotel] = useState([])
	let [MotelsForCity, setMotelsForCity] = useState([])
	let [count, setCount] = useState(0)
	let [noPeticion, setNoPeticion] = useState(false)
	let [toPlay, setToPlay] = useState(true)
	let [habitacionImage, setHabitacionImage] = useState([])
	let [array, setArray] = useState([])
	let [translate, setTranslate] = useState(0);    
	let [preload, setPreload] = useState(false);

	let ip = 'http://192.168.1.4:8000/'

	function toggleFullScreen() {
	  if (!document.fullscreenElement) {
	      document.documentElement.requestFullscreen();
	  }else {
	    if (document.exitFullscreen) {
	      	document.exitFullscreen(); 
	    }
	  }
	}

	return(
		<Provider value={{filter, setFilter,historyList, setHistoryList, isOpenModal, setIsOpenModal, 
		 id, setId, Historias, setHistorias, HistoryMotel, setHistoryMotel, MotelsForCity,
		 setMotelsForCity, toggleFullScreen, count, setCount, noPeticion, setNoPeticion, toPlay, setToPlay,
		 ip, habitacionImage, setHabitacionImage,array, setArray, translate, setTranslate, preload, setPreload
		}}>
			{children}
		</Provider>
	)
}

export {Context, Consumer, TheContext}