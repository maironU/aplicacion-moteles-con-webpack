import {createGlobalStyle} from 'styled-components'

const GlobalStyled = createGlobalStyle`
	body {
		font-family: 'Roboto', sans-serif;
		margin: 0;
		padding: 0;
		background: #EDEDEF;
		box-sizing: border-box;
	}
`
export default GlobalStyled