import React, {useState, useEffect, useContext} from 'react'
import InfoBar from '../Components/InfoBar'
import {TheContext} from '../Context'
import GalleryImages from '../Components/GalleryImages'
import MotelInformation from '../Components/MotelInformation'
import TypesOfRooms from '../Components/TypesOfRooms'
import Services from '../Components/Services'
import RomanticsPlans from '../Components/RomanticsPlans'
import Contacts from '../Components/Contacts'
import Preloader from '../Components/Preloader'


import {
  useParams
} from "react-router-dom";

const InfoMotel = () => {

	let {images, setImages, ip, setHabitacionImage, setPreload} = useContext(TheContext)
	let { motel } = useParams()
	let nombreMotel = motel.replace(/-/gi, " ")
	let [contacto, setContacto] = useState()

	useEffect(() => {

		const abortController =  new AbortController()
		const signal = abortController.signal

		if(contacto === undefined){
			setPreload(true)
		}	

		window.fetch(ip + `api/contacto/${nombreMotel}`, {signal: signal})
		.then(function(res){
			return res.json()
		})
		.then(function(myJson){
			setContacto(myJson)
			setPreload(false)
			console.log(myJson[0].motel_id)
		})
		.catch(function(error){
			console.log("abortado")
		})
		return () => abortController.abort()
	},[])

	return(
		<div style = {{background: 'white'}}>
			<Preloader />

			{contacto && 
				<>
					<InfoBar motel = {nombreMotel} />
					<GalleryImages id = {contacto[0].motel_id}/>
					<MotelInformation id = {contacto[0].motel_id}/>
					<TypesOfRooms id = {contacto[0].motel_id}/>
					<Services id = {contacto[0].motel_id} />
					<RomanticsPlans id = {contacto[0].motel_id}/>
					<Contacts numbercell = {contacto[0].numbercell} numberphone= {contacto[0].numberphone} address = {contacto[0].address}/>
				</>	
			}
		</div>
	)
}

export default InfoMotel
