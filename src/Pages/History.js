import React, {useEffect, useState, useContext} from 'react';
import Modal from '../Components/Modal'
import {TheContext} from '../Context'
import Preloader from '../Components/Preloader'

import {
  useParams
} from "react-router-dom";

function History() {

	let { setPreload, noPeticion,setNoPeticion, Historias, setHistorias, setHistoryMotel, toggleFullScreen, setToPlay, ip} =useContext(TheContext)

	let { motel } = useParams()

	let nombreMotel = motel.replace(/-/gi, " ")	

	useEffect(() => {

		const abortController =  new AbortController()
		const signal = abortController.signal

		if(Historias.length === 0){
			setPreload(true)
		}	

		if(noPeticion === false){
			window.fetch(ip + `api/historiaMoteles/mostrar/${nombreMotel}`, {signal: signal})
			.then(function(res){
				return res.json();
			})
			.then(function(myJson){
				setHistorias(myJson[0].motel_history);
				setPreload(false)
				setHistoryMotel(myJson[0])
				setNoPeticion(true)
				setToPlay(false)
			})
			.catch(function(error){
				console.log("abortado")
			})
		}	

		return () => abortController.abort()
	},[])

  return (
    <>  
		<Preloader />
		{noPeticion === true &&
    		<Modal />
    	}
    </>
  );
}

export default History;
