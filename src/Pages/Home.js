import React from 'react'
import SearchBar from '../Components/SearchBar'
import MotelList from '../Components/MotelList'
import HistoryList from '../Components/HistoryList'
import Footer from '../Components/Footer'
import Preloader from '../Components/Preloader'

const Home = () => {

	return(
		<React.Fragment>
				<Preloader />

				<SearchBar />
			
				<HistoryList />

				<MotelList />

				<Footer />	

		</React.Fragment>
	)
}

export default Home
