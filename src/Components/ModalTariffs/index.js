import React, {useContext, useEffect} from 'react'
import ReactDOM from 'react-dom'
import {TheContext} from '../../Context'
import {ContainerModal, Modal, ContainerTarifas, Tarifa, Salir, Texto} from './styled'

const ModalTariffs = ({tarifas, setIsOpenModalTariffs}) => {

	useEffect(() => {
		return () => closeOpenModal()
	},[])

	const closeOpenModal = () => {
		var overflow = document.getElementById("overflow")
		overflow.style.overflow = "auto"
		setIsOpenModalTariffs(false)
	}

	console.log(tarifas)

	return ReactDOM.createPortal(
		<ContainerModal>
			<Modal>
				<Texto>Tarifas</Texto>
				<ContainerTarifas>
					{tarifas.map(function(item, key){
						return(
							<Tarifa key={key}>{item.tariff}</Tarifa>
						)
					})}
				</ContainerTarifas>
				<Salir onClick={() => closeOpenModal()}><b>Salir</b></Salir>
			</Modal>
		</ContainerModal>,
		document.getElementById("tarifas")
	)
}

export default ModalTariffs