import styled from 'styled-components'

export const ContainerModal = styled.div`
  position: fixed;
  top: 0;
  width:100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1000;
`
export const Modal = styled.div`
  position:fixed;
  background: white;
  width: 85%;
  height: auto;
  border-radius: 15px;
`
export const Texto = styled.div`
  width: 100%;
  text-align: center;
  padding: 20px 0 0 0;
  font-size: 18px;
  font-weight: 700;
  color: rgba(0,0,0,0.98);
 `
export const ContainerTarifas = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  font-size: 12px;
  color: #a5a5a5;
`
export const Tarifa =  styled.div`
  width: 100%;
  border-bottom: 0.3px solid #a5a5a5;
  text-align: center;
  padding: 15px 0 0 0;
`
export const Salir = styled.div`
  font-size: 14px;
  width: 100%;
  text-align: center;
  padding: 20px 0 20px 0;
  color: #334583;
`