import styled, {keyframes} from 'styled-components'

export const ContainerModal = styled.div`
	background: black;
	color: #fff;
	position: fixed;
	width:100vw;
    height:100vh;
`

export const Container = styled.div`
	width: 100%;
	height: 100%;
	position: relative;

	@media (min-width: 768px){
		width: 370px;
		margin: auto;
	}
`

export const ContainerAll = styled.div`
	margin: 12px auto;
	width: 90%;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
`
export const ContainerLogo = styled.div`
	align: center;
	width: 100%;
	display: flex;
	flex-direction: row;
	font-size: 13px;
	align-items: center;
`
export const Logo = styled.img`
	width: 30px;
	height: 30px;
	border-radius: 50%;
`
export const Name = styled.span`
	margin-left: 10px;
`
export const Date = styled.span`
	color: grey;
	margin-left: 10px;
`
export const ContainerContentHistory = styled.div`
	margin: 0;
	width: 100%;
	display: flex;
	flex-flow: row wrap;
	height: 100%;
	overflow: hidden;
	position: absolute;	

`
export const ContainerImg = styled.div`
	margin: 0;
	width: 100%;
	height: 80%;
	opacity: ${props => props.opacity};
`
const transicion = keyframes`
  0% {filter:blur(6px);} 100% {filter: blur(0);}`

export const ImgContentHistory = styled.img`
	width: 100%;
	height: 100%;
	margin-left; 0;
	transition: all 8s;
	object-fit: cover;
`
export const ContainerVideo = styled.div`
`
export const VideoContentHistory = styled.video`
`
export const ContainerArrows = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
`

export const MoveLeft = styled.div`
	width: 30%;
	height: 60%;
	position: relative;
	bottom: 85%;
`

export const MoveRight = styled.div`
	width: 30%;
	height: 60%;
	position: relative;
	bottom: 85%;
	right: 0;
`
export const ContainerToPlay = styled.div`
	position: absolute;
	right: 50%;
	top: 40%;
`
