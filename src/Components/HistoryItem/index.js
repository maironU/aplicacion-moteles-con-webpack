import React, {useContext} from 'react'
import {ContainerItem, Image, Name, ContainerName, ContainerAll, Link} from './styled'
import {TheContext} from '../../Context'

const HistoryItem = ({name, urlImage, motel_history}) => {

	let {setId, setIsOpenModal, setHistorias, setHistoryMotel, toggleFullScreen, setNoPeticion, ip} = useContext(TheContext)

	const showHistory = () => {
		toggleFullScreen()
		setHistorias(motel_history)
		setHistoryMotel({name, urlImage})
		setIsOpenModal(true)
		setNoPeticion(true)
	}

	return(
		<>
		{motel_history.length > 0 &&
			<ContainerItem>
				<ContainerAll>
					<Link onClick={(e) => showHistory()} to={`/historia/${name.replace(/ /g, "-")}`}>
						<Image src = {ip + `api/almacenamiento/${urlImage}`} />
					</Link>
				</ContainerAll>

				<ContainerName>
					<Name>{name}</Name>
				</ContainerName>		
			</ContainerItem>
		}
		</>
	)
}

export default HistoryItem