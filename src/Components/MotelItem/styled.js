import styled, {keyframes} from 'styled-components'
import {Link as LinkRouter} from 'react-router-dom';

export const Link = styled(LinkRouter)`	
	width: 100%;
	height: 250px;
	list-style: none;
	position: relative;
	display: flex;
	flex-direction: column;
	margin-top: 6px;
	text-decoration: none;

	@media (min-width: 768px) and (max-width: 991px) {
		width: 48%;
		height: 250px;
		margin: 15px 1% 0 1%;
	}

	@media (min-width: 992px) and (max-width: 1199px) {
		width: 48%;
		height: 300px;
		margin: 15px 1% 0 1%;
	}

	@media (min-width: 1200px) {
		width: 48%;
		height: 350px;
		margin: 15px 1% 0 1%;
	}
`
export const Price = styled.span`

`
const transicion = keyframes`
  0% {filter:blur(6px);} 100% {filter: blur(0);}
`
export const Image = styled.img`
	width: 100%;
	height: 80%;
	filter: blur(6px);
	animation: ${transicion} 0.5s linear;
	animation-fill-mode: forwards;
	object-fit: cover;
`
export const Name = styled.span`
	font-weight: 700;
	font-size: 16px;
	color: #fff;

	@media (min-width: 991px) and (max-width: 1199px) {
		font-size: 18px;
	}

	@media (min-width: 1200px) {
		font-size: 19px;
	}
	
`
export const ContainerPrice = styled.div`
	font-size: 14px;
	display: inline-block;
	padding: 10px;
	color: #fff;
	font-weight: 500;
	background: rgba(0, 0, 0, 0.55);
	position: absolute;
	left: 0;

	@media (min-width: 992px) and{
		font-size: 17px;
		padding: 12px;
	}

`
export const ContainerName = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	justify-content: space-between;
	padding: 4px;
	background-color: red; 
 	background: -moz-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -webkit-gradient(left top,right top,color-stop(0%,rgba(51,68,131,1)),color-stop(100%,rgba(110,35,104,1)));
    background: -webkit-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -o-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -ms-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: linear-gradient(to right,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    -webkit-filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#334483',endColorstr='#6e2368',GradientType=1 );
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#334483',endColorstr='#6e2368',GradientType=1 );
	
	@media (min-width: 1200px) {
		padding: 6px;
	}
`
export const ContainerArrowRight = styled.div`
	display: flex;
	align-items: center;
`
