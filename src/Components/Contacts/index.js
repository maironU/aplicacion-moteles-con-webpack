import React from 'react'
import { ContainerContacts, TextContactar, ContainerContact, ContainerLogoNumberCell,
    ContainerLogoNumberPhone, ContainerLogoAddress, TextNumeroCell, TextNumeroPhone, TextAddress  } from './styled'
//Logos
import { MdPhoneIphone, MdPhoneInTalk} from 'react-icons/md'
import { FaRegAddressCard } from 'react-icons/fa'



const Contacts = ({numberphone, numbercell, address}) => {
    console.log(numberphone)
    return(
        <ContainerContacts>
            <TextContactar>Contactar</TextContactar>
            <ContainerContact>
                <ContainerLogoNumberCell>
                    <MdPhoneIphone />
                    <TextNumeroCell>
                        +57 {numbercell}
                    </TextNumeroCell>
                </ContainerLogoNumberCell>

                <ContainerLogoNumberPhone>
                    <MdPhoneInTalk />
                    <TextNumeroPhone>
                        035 {numberphone}
                    </TextNumeroPhone>
                </ContainerLogoNumberPhone>
                
                <ContainerLogoAddress>
                    <FaRegAddressCard />
                    <TextAddress>
                        {address}
                    </TextAddress>
                </ContainerLogoAddress>
            </ContainerContact>
        </ContainerContacts>
    )
}

export default Contacts