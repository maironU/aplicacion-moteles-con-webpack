import styled from 'styled-components'

export const ContainerContacts = styled.div`
    width: 95%;
    margin: 0 auto;
    padding: 10px 0 10px 0;
`
export const TextContactar = styled.span`
    font-size: 19px;
    color: #121a2f;
    font-weight: 500;
`
export const ContainerContact = styled.div`
    margin-top: 10px;
    font-size: 10px;
    color: #707070;
    display: flex;
    flex-direction: column;
`
export const ContainerLogoNumberCell = styled.div`
    display: flex;
`
export const ContainerLogoNumberPhone = styled.div`
`
export const ContainerLogoAddress = styled.div`
`
export const TextNumeroCell = styled.span`
    margin-left: 10px;
`
export const TextNumeroPhone = styled.span`
    margin-left: 10px;
`
export const TextAddress = styled.span`
    margin-left: 10px;
`

