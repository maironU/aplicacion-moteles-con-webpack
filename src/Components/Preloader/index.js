import React, {useContext} from 'react'
import { ContainerPreload, Preload } from './styled'
import {TheContext} from '../../Context'

const Preloader = () => {

	let {preload} = useContext(TheContext)


    return(
        <>
            {preload === true &&
                <ContainerPreload>
                    <Preload>

                    </Preload>
                </ContainerPreload>
            }
        </>
    )
}

export default Preloader