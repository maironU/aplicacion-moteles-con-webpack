import styled, {keyframes} from 'styled-components'

const animationPreloader = keyframes`
    from {
        transform: rotate(0deg);
    }
    to {
        transform: rotate(360deg);
    }
`
export const ContainerPreload = styled.div`
    position: fixed;
    top: 0;
    width:100%;
    height: 100%;
    z-index: 1000;
`

export const Preload = styled.div`
    position: absolute;
    top: calc(50% - 34px);
    right: calc(50% - 34px);
    width: 40px;
    height: 40px;
    border: 6px solid #eee;
    border-radius: 50%;
    border-top: 6px solid #666;
    animation-name: ${animationPreloader};
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
`