import styled from 'styled-components'

export const ContainerInfoBar = styled.div`
	min-height: 25px;
	width: 100%;
	position: fixed;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
	background: white;
	padding: 10px;
	top: 0;
	color: #707070;
	z-index: 1000;
	@media (min-width: 768px) and (max-width: 991px) {
		padding: 15px;
	}
`
export const ContainerBackName = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
`
export const Name = styled.span`
	@media (min-width: 768px) and (max-width: 991px) {
		font-size: 20px;
	}
`
export const Share = styled.div`
	margin-right: 25px;
`
export const ContainerBackArrow = styled.div`
	display: flex;
	align-items: center;
`
