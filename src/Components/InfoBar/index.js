import React from 'react'
import {ContainerInfoBar, ContainerBackName, Name, Share, ContainerBackArrow } from './styled'
import { MdKeyboardArrowLeft} from 'react-icons/md'
import { FiShare} from 'react-icons/fi'

const InfoBar = ({motel}) =>{
	function Compartir() {
        if (navigator.share) {
			navigator.share({
			  title: 'My awesome post!',
			  text: 'Te comparto esta url, ábrela',
			  url: window.location.href
			}).then(() => {
			  console.log('Thanks for sharing!');
			})
			.catch(err => {
			  console.log(`Couldn't share because of`, err.message);
			});
		}else {
			console.log('web share not supported');
		}
	}

	return(
		<ContainerInfoBar>
			<ContainerBackName>
				<ContainerBackArrow onClick = {() => window.history.back()}>
					<MdKeyboardArrowLeft size={25}/>
				</ContainerBackArrow>
				<Name>Motel {motel}</Name>
			</ContainerBackName>

			<Share onClick = {() => Compartir()}>
				<FiShare size={18}/>
			</Share>
		</ContainerInfoBar>
	)
}

export default InfoBar