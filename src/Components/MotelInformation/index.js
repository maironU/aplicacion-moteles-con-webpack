import React, {useEffect, useState, useContext} from 'react'
import { ContainerInformation, Information } from './styled'
import {TheContext} from '../../Context'

const MotelInformation = ({id}) => {
	

	let {ip} = useContext(TheContext)
	let [information, setInformation] = useState("");

	useEffect(() => {

		const abortController =  new AbortController()
		const signal = abortController.signal

		window.fetch(ip + `api/informacion/${id	}`, {signal: signal})
		.then(function(res){
			return res.json()
		})
		.then(function(informacion){
			setInformation(informacion)
		})
		.catch(function(error){
			console.log("abortado")
		})
		
		return () => abortController.abort()
	},[])

	return(
		<>
		{information.length > 0 &&
			<ContainerInformation>
				<Information>
					{information}
				</Information>
			</ContainerInformation>
		}
		</>
	)
}

export default MotelInformation