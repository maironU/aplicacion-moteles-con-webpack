import styled from 'styled-components'
	
export const ContainerInformation = styled.div`
	width: 100%;
	margin-top: 50px;
	min-height: 200px;
	padding-bottom: 10px;
	border-bottom: 0.7px solid #d7d7d7;
	@media (min-width: 768px) and (max-width: 991px) {
		margin-top: 77px;
		font-size: 25px;
	}
`

export const Information = styled.p`
	width: 94%;
	margin: 0 auto;
	text-align: justify;
	color: #707070;
`