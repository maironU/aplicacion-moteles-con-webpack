import React, {useContext, useEffect} from 'react'
import {ContainerImageItem, Image} from './styled'
import {TheContext} from '../../Context'

const ImageItem = ({mover, imagen}) => {
	
	let {ip, translate} = useContext(TheContext)

	return(
		<>
			<ContainerImageItem mover = {translate}>
				<Image src={ip + `api/almacenamiento/${imagen}`} />
			</ContainerImageItem>
		</>
	)
}

export default ImageItem