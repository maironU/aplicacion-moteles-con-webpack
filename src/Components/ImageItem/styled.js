import styled, {keyframes} from 'styled-components'

const transicion = keyframes`
  0% {filter:blur(6px);} 100% {filter: blur(0);}
`

export const ContainerImageItem = styled.li`
	min-width: 100%;
	min-height: 100%;
	background: white;
	transform: translateX(${props => props.mover}%);
    transition-duration: 0.5s;
	transition-timing-function: ease-in-out;

	@media (min-width: 768px) and (max-width: 991px) {
		min-width: 50%;
		box-sizing: border-box;
		padding-right: 3px;
	}
`

export const Image = styled.img`
	width: 100%;
	height: 100%;
	filter: blur(6px);
	animation: ${transicion} 0.5s linear;
	animation-fill-mode: forwards;
	object-fit: cover;
`

