import React, {useState} from 'react'
import { ContainerFooter, Logo } from './styled'

const Footer = () => {

    let [toogle, setToogle] = useState(true);

    const openFooter = () => {
        if(toogle){            
            var footer =  document.getElementById("toogle");
            footer.style.transitionDuration = "0.5s";
            footer.style.transitionTimingFunction  = "linear";
            footer.style.bottom = "0";

            setToogle(!toogle);
        }else{
            var footer =  document.getElementById("toogle");
            footer.style.transitionDuration = "0.5s";
            footer.style.transitionTimingFunction  = "linear";
            footer.style.bottom = "-27vh";

            setToogle(!toogle);
        }
    }

    return(
        <>
            <ContainerFooter id = "toogle" onClick = { () => openFooter()}>
                <Logo>

                </Logo>
            </ContainerFooter>
        </>
    )
}

export default Footer;