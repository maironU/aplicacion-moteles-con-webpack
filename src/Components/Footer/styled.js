import styled, {keyframes} from 'styled-components'

export const ContainerFooter = styled.footer`
    width: 100vw;
    height: 30vh;
    background: #1a1a1a;
    position: fixed;
    bottom: -27vh;
    left: 0;
    right: 0;
    border-radius: 10px 10px 0 0;
`
export const Logo = styled.div`
    width: 0;
    height: 0;
    border-left: 13px solid transparent;
    border-right: 13px solid transparent;
    border-bottom: 16px solid #1a1a1a;
    position: relative;
    bottom: 10px;
    left: calc(50vw - 12px); 
`