import React, {useContext, useState, useEffect} from 'react'
import { ContainerServices, TextServicios, ContainerAllServices, ContainerServicio, Imagen, Name } from './styled'
import {TheContext} from '../../Context'
const Services = ({id}) => {

	let [servicios, setServicios] = useState([])

	useEffect(() => {

		const abortController =  new AbortController()
		const signal = abortController.signal

		window.fetch(ip + `api/servicios/mostrar/${id}`, {signal: signal})
		.then(function(res){
			return res.json()
		})
		.then(function(myJson){
			setServicios(myJson)
		})
		.catch(function(error){
			console.log("abortado")
		})
		
		return () => abortController.abort()
	},[])

	let {ip} = useContext(TheContext)
	return(
		<ContainerServices>
			<TextServicios>Servicios</TextServicios>
			<ContainerAllServices>
				{servicios.map(function(item, key){
					return(
						<ContainerServicio key = {key}>
							<Imagen src ={ ip + `api/almacenamiento/${item.urlservicio}`} />
							<Name>{item.nameservicio}</Name>
						</ContainerServicio>
					)
				})}
			</ContainerAllServices>
		</ContainerServices>
	)
}

export default Services