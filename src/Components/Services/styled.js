import styled from 'styled-components'

export const ContainerServices = styled.div`
	width: 95%;
	margin 15px auto;
`
export const TextServicios = styled.span`
	color: #121a2f;
	font-size: 23px;
	font-weight: 500;
`
export const ContainerAllServices = styled.div`
	width: 100%;
	display: flex;
	flex-flow: row wrap;
	margin-top: 10px;
	margin-bottom: 10px;
`
export const ContainerServicio = styled.div`
	width: 30%;
	background: #f2f2f2;
	display: flex;
	flex-direction: column;
	height: 90px;
	align-items: center;
	margin-bottom: 20px;
	border-radius: 5px;
	justify-content: center;
	box-shadow: 3px 3px 7px rgba(0,0,0,0.15);
	border: 1px solid rgba(0,0,0,0.125);
	margin: 0 1% 15px 1.5%;

	@media (min-width: 768px) and (max-width: 991px) {
		height: 130px;
	}
`
export const Imagen = styled.img`
	width: 20px;
	height: 20px;
	margin-bottom: 10px;
`
export const Name = styled.span`
	color: #121a2f;
	font-size: 10px;
`