import React, {useState, useEffect, useContext} from 'react'
import { ContainerRomanticsPlans, TextPlanesRomanticos, ContainerRomanticPlan, ContainerTriangle, 
	Triangle, ContainerInfoPlan, Plan } from './styled'
import {TheContext} from '../../Context'

const RomanticsPlans = ({id}) => {

	let {ip} = useContext(TheContext)

	let [planes, setPlanes] = useState([])

	useEffect(() => {
		const abortController =  new AbortController()
		const signal = abortController.signal

		window.fetch(ip + `api/planes/mostrar/${id}`, {signal: signal})
		.then(function(res){
			return res.json()
		})
		.then(function(myJson){
			setPlanes(myJson)
		})
		.catch(function(error){
			console.log("abortado")
		})
		
		return () => abortController.abort()
	},[])

	return(
		<ContainerRomanticsPlans>
			<TextPlanesRomanticos>Planes Románticos</TextPlanesRomanticos>
			{planes.map(function(item, key){
				return(
					<ContainerRomanticPlan key = {key}>
						<ContainerTriangle>
							<Triangle></Triangle>
						</ContainerTriangle>

						<ContainerInfoPlan>
							<Plan>{item.romanticplan}</Plan>
						</ContainerInfoPlan>
					</ContainerRomanticPlan>
				)
			})}
		</ContainerRomanticsPlans>
	)
}

export default RomanticsPlans