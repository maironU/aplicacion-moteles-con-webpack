import styled from 'styled-components'
//import {Link as LinkRouter} from 'react-router-dom';

export const ContainerImagenHabitacion = styled.div`
	min-width: 80%;
	height: auto;
	object-fit: cover;
	margin-right: 20px;
	@media (min-width: 768px) and (max-width: 991px) {
		min-width: 45%;
	}
`

export const ImgHabitacion = styled.img`
	width: 100%;
	height: 170px;
	object-fit: cover;
	border-radius: 5px;
	@media (min-width: 768px) and (max-width: 991px) {
		height: 230px;
	}
`
export const ContainerName = styled.div`
	font-size: 18px;
	font-weight: 300;
`
export const Name = styled.span`
`