import React, {useContext, useEffect, useState} from 'react'
import { ContainerImagenHabitacion, ImgHabitacion, ContainerName, Name } from './styled'
import {TheContext} from '../../Context'

const Habitaciones = ({namehabitacion, imgprofile, habitacion_image}) => {

	let {habitacionImage, setHabitacionImage, setArray, setTranslate, ip} = useContext(TheContext)

	const ImagesHabitacion = () => {
		console.log(habitacion_image)
		setArray(habitacion_image)

		$(document).ready(function() {
			$('html, body').animate({scrollTop:0}, 'slow'); 
			return false;
		});

		setTranslate(0)
	}

	return(
		<ContainerImagenHabitacion onClick = {()=> ImagesHabitacion()}>
			<ImgHabitacion src = {ip + `api/almacenamiento/${imgprofile}`} />

			<ContainerName>
				<Name>Habitación {namehabitacion}</Name>
			</ContainerName>
		</ContainerImagenHabitacion>
	)
}

export default Habitaciones