import React, {useState, useEffect, useContext} from 'react'
import {List, ContainerPrice, Price, Arrow} from './styled'
import ImageItem from '../ImageItem'
import {TheContext} from '../../Context'
import Hammer from 'hammerjs';
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi'

const GalleryImages = ({id}) => {

	let {array, ip, translate, setTranslate} = useContext(TheContext)

	let [images, setImages] = useState([])
	let [ref, setRef] = useState(React.createRef());
	let [display, setDisplay] = useState("auto");

	/*useEffect(() => {
		let id = document.getElementById("ul")
		if(id){
			id.scrollLeft = 0
		}
	},[array])*/

	useEffect(() => {

		if(images.length > 0){
			if(window.screen.width >= 768	){
				if(translate === (images[0].image.length - 2)*(-100)){
					setDisplay("none");
				}else {
					setDisplay("auto");
				}
			}
		}

		if(images.length > 0){
			if(window.screen.width < 768 ){
				if(translate === (images[0].image.length - 1)*(-100)){
					setDisplay("none");
				}else{
					setDisplay("auto");
				}
			}
		}

        let hammer = new Hammer(ref.current)

        hammer.on('swipeleft swiperight', event => {
            let type = event.type
            
            if (type === 'swipeleft')  {
                MoveRight()
            }
            
            else if (type === 'swiperight') {
                MoveLeft()
            }
        })

        return () => hammer.destroy()

    }, [translate])

	
	useEffect(() => {

		const abortController =  new AbortController()
		const signal = abortController.signal

		window.fetch(ip + `api/imagenes/${id}`, {signal: signal})
		.then(function(res){
			return res.json()
		})
		.then(function(myJson){
			setImages(myJson)
			console.log(myJson)
		})
		.catch(function(error){
			console.log("abortado")
		})
		
		return function cleanup() {
			setTranslate(0)
			abortController.abort()
		} 
	},[])
	
	const MoveLeft = () => {
		console.log(translate)
        if(translate === 0){
				var left = document.getElementById("left");
				left.style.pointerEvents = "none";
        }else{
            document.getElementById("left").style.pointerEvents = "auto";
            document.getElementById("right").style.pointerEvents = "auto";
            setTranslate(translate + 100);
        }
    }

    const MoveRight = () => {
		console.log(window.screen.width)
		console.log(translate)
		if(images.length > 0){
			var tamano = images[0].image.length - 1;
		}

        if(translate === tamano*(-100)){
			var right =  document.getElementById("right")
			right.style.pointerEvents = "none";
        }else{
            document.getElementById("right").style.pointerEvents = "auto";
            document.getElementById("left").style.pointerEvents = "auto";
            setTranslate(translate - 100);
        }
    }

	return(
		<>
		<List ref = {ref}>
				{images.length > 0 &&
					<>
						<ContainerPrice>
							<Price>${images[0].price} - ${images[0].pricefinal}</Price>
						</ContainerPrice>

						{array &&
							<>
								{array.map( (elem, key ) => <ImageItem key={key} {...elem}/> ) }
							</>
						}
						{ images[0].image.map( (elem, key ) => <ImageItem key={key} {...elem}/> ) }

						<Arrow display = { translate === 0 ? "none": "auto"} property = "left: 0" id = "left" onClick = { () => MoveLeft() } margin = "margin-left: 8px"><FiChevronLeft size ={15} /></Arrow>
						<Arrow display = {display} property = "right: 0" id= "right" onClick = { () => MoveRight() }  margin = "margin-right: 8px"><FiChevronRight size ={15}/></Arrow>
					</>
				}
		</List> 
		</>
	)
}

export default GalleryImages