import styled from 'styled-components'

export const List = styled.ul`
	position: sticky;
	margin: 0;
	padding: 0;
	width: 100%;
	height: 270px;
	display: flex;
	top: 45px;
	display: flex;
    overflow: hidden;
	position: relative;
	
	@media (min-width: 768px) and (max-width: 991px) {
		top: 59px;
		height: 350px;
	}

`

export const ContainerPrice = styled.div`
	display: inline-block;
	padding: 10px;
	color: white;
	background: rgba(0, 0, 0, 0.55);
	position: absolute;
	left: 0;
	top: 0;
	z-index: 100;
`
export const Price = styled.span`
	font-weight: 500;
	font-size: 14px;
	color: #fff;
`

export const Arrow = styled.div`
	width: 25px;
	height: 25px;
    position: absolute;
	${props => props.property};
	bottom: 50%;
    cursor: pointer;
	font-size: 40px;
	background: white;
	border-radius: 50%;
	display: flex;
	justify-content: center;
	align-items: center;
	display : ${props => props.display};
	${props => props.margin};
`


