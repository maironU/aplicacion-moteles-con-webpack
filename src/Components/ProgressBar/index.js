import React, {useContext} from 'react'
import {ContainerBar} from './styled'
import {TheContext} from '../../Context'
import Bars from '../Bars'

const CProgressBar = ({cont}) => {

	let {Historias} = useContext(TheContext)

	return(
		<ContainerBar>
			{Historias.map((elem, key) =><Bars cont={cont} id={key} key={key} />)}
		</ContainerBar>
	)
}

export default CProgressBar