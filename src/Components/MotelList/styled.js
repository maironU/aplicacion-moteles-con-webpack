import styled from 'styled-components'

export const List = styled.ul`
	width: 100%;
	margin: 0;
	padding: 0;
	background: #EDEDEF;

	@media (min-width: 768px){
		display: flex;
		flex-flow: row wrap;
	}
`
