import React , {useContext, useState, useEffect} from 'react'
import {List} from './styled'
import MotelItem from '../MotelItem'
import {TheContext} from '../../Context'

const MotelList = () => {

	let {filter, MotelsForCity, setMotelsForCity, ip, setPreload} = useContext(TheContext)

	useEffect(() => {

		const abortController =  new AbortController()
		const signal = abortController.signal

		if(MotelsForCity.length === 0){
			setPreload(true)
		}		
		const dato = JSON.stringify(filter.city_id) === undefined? '': JSON.stringify(filter.city_id)

		window.fetch(ip + `api/moteles/mostrar/${dato}`, {signal: signal})
		.then(function(res){
			return res.json();
		})
		.then(function(myJson){
			setMotelsForCity(myJson);
			setPreload(false)

		})
		.catch(function(error){
			console.log("abortado")
		})
		
		return function cleanup() {
			setPreload(false)
			abortController.abort()
		}
	},[filter])


	//`http://192.168.1.4:8000/api/almacenamiento/${motel.urlImage}`

	return(
		<List>
            { MotelsForCity.map( (elem, key ) => <MotelItem key={key} {...elem}/> ) }
        </List>
	)
}

export default MotelList