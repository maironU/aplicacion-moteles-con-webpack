import React, {useState, useContext, useEffect} from 'react'
import Habitaciones from '../Habitaciones'
import {ContainerAllTipos, ContainerTipo, TextHabitaciones, Tipo, ContainerNamePrice, Name, ContainerPriceTariffs,
	 Price, Tariffs, ContainerHabitaciones} from './styled'
import {TheContext} from '../../Context'
import ModalTariffs from '../ModalTariffs'

const TypesOfRooms = ({id}) => {

	let {ip} = useContext(TheContext)

	let [isOpenModalTariffs, setIsOpenModalTariffs] = useState(false)
	let [tarifas, setTarifas] = useState([])
	let [tiposDeHabitaciones, setTiposDeHabitaciones] = useState([])

	useEffect(() => {
		const abortController =  new AbortController()
		const signal = abortController.signal

		window.fetch(ip + `api/tipoHabitaciones/mostrar/${id}`, {signal: signal})
		.then(function(res){
			return res.json()
		})
		.then(function(tipos){
			setTiposDeHabitaciones(tipos)
		})
		.catch(function(error){
			console.log("abortado")
		})
		
		return () => abortController.abort()
	},[])

	const isOpenModal = ($tarifas) => {
		var overflow = document.getElementById("overflow")
		overflow.style.overflow = "hidden"
		setIsOpenModalTariffs(true)
		setTarifas($tarifas)
	}

	return(
		<>
			<ContainerAllTipos>
				<ContainerTipo>
					<TextHabitaciones>Habitaciones</TextHabitaciones>
					{tiposDeHabitaciones.map(function(item, key){
						return(
							<Tipo  key={key}>
								<ContainerNamePrice>
									<Name>
										{item.tipo_habitacion}	
									</Name>
									<ContainerPriceTariffs>
										<Price>
											${item.price}/h
										</Price>
										<Tariffs onClick = {() => isOpenModal(item.tariff)}>Tarifas</Tariffs>
									</ContainerPriceTariffs>
								</ContainerNamePrice>
								<ContainerHabitaciones>
									{item.habitacion.map((item, key) => <Habitaciones key={key} {...item} />)}
								</ContainerHabitaciones>
							</Tipo>
						)
					})}
				</ContainerTipo>
			</ContainerAllTipos>
			{isOpenModalTariffs &&
				<ModalTariffs tarifas = {tarifas} setIsOpenModalTariffs = {setIsOpenModalTariffs}/>
			}
		</>
	)
}

export default TypesOfRooms