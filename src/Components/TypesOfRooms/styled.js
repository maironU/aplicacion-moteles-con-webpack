import styled from 'styled-components'

export const ContainerAllTipos = styled.div`
	width: 100%;
	border-bottom: 0.7px solid #d7d7d7;
`
export const ContainerTipo = styled.div`
	width: 95%;
	margin: 15px auto;
	display: flex;
	flex-direction: column;
`
export const TextHabitaciones = styled.span`
	color: #121a2f;
	font-size: 23px;
	font-weight: 500;
`
export const Tipo = styled.div`
	margin-bottom: 10px;
`
export const ContainerNamePrice = styled.div`
	font-size: 13px;
	display: flex;
	justify-content: space-between;
	margin-bottom: 10px;
	margin-top: 10px;
`
export const Name = styled.span`
	font-size: 13px;
	font-weight: 500;
	color: #707070;
`
export const ContainerPriceTariffs = styled.div`
	display: flex;
	align-items: center;
`
export const Tariffs = styled.div`
	cursor: pointer;
	color: #334583;
	font-size: 14px;
	font-weight: 700;
`
export const Price = styled.span`
	font-size: 10px;
	color: #121a2f;
	font-weight: 500;
	font-style: italic;
	margin-right: 5px;
`
export const ContainerHabitaciones = styled.div`
	width: 100%;
	display: flex;
	overflow: scroll;

	::-webkit-scrollbar {
    	display: none;
	}
`
